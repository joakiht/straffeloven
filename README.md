# Straffeloven 2005 - se [lovdata](https://lovdata.no/dokument/NL/lov/2005-05-20-28)
* Loven kan anvendes f.eks ved:
    * § 204. Innbrudd i datasystem
    * § 207. Krenkelse av forretningshemmelighet
    * § 351. Skadeverk
      * For skadeverk straffes også den som uberettiget endrer, gjør
      * tilføyelser til, ødelegger, sletter eller skjuler andres data.
